# Node Express Sessions 101

A very quick sample project on Express with Sessions

## Install

Clone the repo and run `npm install` in the root of the directory. 

## Run

The project uses nodemon to execute app. You can start the project using `npm start` from the root directory of the project.