const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');


// Enable Request Cookies
app.use(cookieParser());

// Enable Server Sessions
app.use(session({
    secret: 'a912jasd-12398asdh=',
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false, // Allows session to work on HTTP
        httpOnly: true, // Disables this cookie for document.cookie
        maxAge: 3600000 // 1 Hour
    }
}));

// Serve my static content (js, css and images);
app.use('/public', express.static( path.join( __dirname, 'www' ) ));

app.get('/', (req, res) => {
    console.log(req.session);
    if (req.session.counter == undefined) {
        res.send(' No session exists for this user... ');
    } else {
        req.session.counter += 1;
        res.send( 'Number of site visits for this user: ' + req.session.counter )
    }    
});

app.get('/create', (req, res)=>{
    req.session.counter = 0;
    res.send('Session was created...');
});

app.listen(3000, () => console.log('App started on Port 3000...'));